# Taller Pragma de Accesibilidad Web
Este proyecto esta basado en el proyecto de [Juliana Gomez](https://github.com/gmzjuliana/curso-acessibilidad-web) del curso de platzi.
## Recursos del taller
Este proyecto es parte de unas charlas dadas en el chapter de Fronteños, se listan aqui los enlances a los videos y las presentanciones:
- [Primeros pasos hacia la Accesibilidad Web Parte 1 (Video)](https://pragma.workplace.com/100064391690669/videos/341602077907629/)
- [Primeros pasos hacia la Accesibilidad Web Parte 1 (Slides)](https://docs.google.com/presentation/d/1bgiqbbk-I4PQzYQea0t4ml6rHIpQJagfIugmxK5QFxU/edit?usp=sharing)
- [Primeros pasos hacia la Accesibilidad Web Parte 2 (Video)](https://pragma.workplace.com/100064391690669/videos/322335693118399/)
- [Primeros pasos hacia la Accesibilidad Web Parte 2 (Slides)](https://docs.google.com/presentation/d/1Nuc81GPSsw-3aj2zhy3EfMVIt3t0vZ1x1g9DzigzsS0/edit?usp=sharing)

## Fases del proyecto
Este proyecto esta organizado en ramas para ver la evolución del código. aqui se listan en orden cada rama:
- [html-Semantico](https://gitlab.com/brandon.arrieta/accesibilidad-web-a11y/-/tree/html-semantico)
- [css-accesible](https://gitlab.com/brandon.arrieta/accesibilidad-web-a11y/-/tree/css-accesible)
- [javascript-accesible](https://gitlab.com/brandon.arrieta/accesibilidad-web-a11y/-/tree/javascript-accesible)

## Referencias y fuentes
- [WCAG 2022](https://www.w3.org/WAI/WCAG22/quickref/)
- [Curso de Accesibilidad Web](https://platzi.com/cursos/accesibilidad-web/)
- [Udacity Intro to Accessibility](https://www.udacity.com/course/web-accessibility--ud891)
- [The A11y Project](https://www.a11yproject.com/)

## Autores
- Brando Arrieta [@brandon369](https://github.com/brandon369)
- William Meza [@wemf](https://github.com/wemf)